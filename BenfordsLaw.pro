#-------------------------------------------------
#
# Project created by QtCreator 2014-02-26T09:55:52
#
#-------------------------------------------------

QT       += core gui
QT += widgets printsupport

TARGET = BenfordsLaw
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h

FORMS    += mainwindow.ui
