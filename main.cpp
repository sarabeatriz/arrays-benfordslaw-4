#include "mainwindow.h"
#include <QApplication>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <sstream>

using namespace std;

//                    //
// SOLUTION EXERCISE 2//
//                    //


// Function to obtain the first digit of a an integer //
int firstDigit(int passenger){
    int quotient;
    
    quotient=passenger/10;
    
    while(quotient > 9){
        quotient=quotient/10;
    }
    return quotient;
}



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    w.setWindowTitle("Benford\'s Law");
    w.show();
    
    // Here is a simple example to help you understand how to
    // invoke the histo method.
    // Aqui esta un ejemplo simple para ayudar a entender como
    // invocar la funcion histo.

    // This is an array of strings
    // Esto es un arreglo de cadenas de caracteres

    // string histoNames[4]  = {"Rosa", "Pepin", "Lobo", "Mota"};

    // This is an array of corresponding values
    // Esto es un arreglo de valores correspondientes

    // double histoValues[4] = {.21, .26, .05, .48};

    // We pass the array of strings, the array of values, the size of the
    // arrays, and the names of the x and y axis.
    // Pasamos el arreglo de cadenas de caracteres, el arreglo de valores,
    // tamano de los arreglos y los nombres de los axis x y y.

    // w.histo(histoNames, histoValues, 4, "personaje", "frecuencia");
    
    //                            //
    // SOULTION EXERCISE 2 (CONT) //
    //                            //
    string bus;
    int numPassenger;
        
    double histoValues[10]={0,0,0,0,0,0,0,0,0,0};
    string histoNames[9]  = {"1","2","3","4","5","6","7","8","9"};
    
    // read data from file; NOTE path has to be modified to your own path //
    ifstream inputFile;
    inputFile.open("/Users/Ivelisse/Dropbox/eip/EIP-REVISION-VERANO/Arrays/BenfordsLaw/benfordslaw-WorkingFolder-WithSolution-July-2015/data/cta-a.txt");
        
    while (inputFile >> bus){
        inputFile >> numPassenger;
        histoValues[firstDigit(numPassenger)-1]= histoValues[firstDigit(numPassenger)-1]+1;
        }
        
    inputFile.close();
        
    // compute frequency of occurrence //
    int sum=0;
    for(int i=0; i<9; ++i)
        sum=sum+histoValues[i];
        
    for(int i=0; i<9; ++i)
        histoValues[i]=histoValues[i]/sum;
    
    // display histogram //
    w.histo(histoNames, histoValues, 9, "Digit", "Frequency");
    
    return a.exec();
}
